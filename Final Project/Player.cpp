//
// Created by sdoherty on 10/11/19.
// Players class to access and set player values of score/name/able to score.
//

#include "Player.h"
#include <string>


using namespace std;
Player::Player()
{
}

// Set player constructor with default values for score and ableToScore
Player::Player(string n) : name(n), score(0), ableToScore(false)
{
}

//Set Players name
void Player::SetName(string name)
{
    name = name;
}
// Get Players name
string Player::GetName()
{
    return name;
}

// Add to Score
void Player::SetScore(int points)
{
    score += points;
}

// Show score
int Player::GetScore()
{
    return score;
}

// set inGame
void Player::SetAbleToScore()
{
    ableToScore = true;
}

// return inGame status
bool Player::GetAbleToScore()
{
    return ableToScore;
}