//
// Created by sdoherty on 10/11/19.
//

#ifndef FINAL_PROJECT_PLAYER_H
#define FINAL_PROJECT_PLAYER_H

#endif //FINAL_PROJECT_PLAYER_H
#pragma once

#include <string>
//Header for Player Class CPP
class Player
{
private:
    //Private member variables
    //string to keep player name
    std::string name;
    //int to track score
    int score;
    //bool to set if player is able to save scores(has +1000 in one roll)
    bool ableToScore;
public:
    //Public constructors and accessors.
    Player();
    Player(std::string name);
    //set and get name values
    void SetName(std::string);
    std::string GetName();
    //set and get score
    void SetScore(int points);
    int GetScore();
    //set and get the ability to add scores.
    void SetAbleToScore();
    bool GetAbleToScore();
};