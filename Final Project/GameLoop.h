//
// Created by sdoherty on 10/11/19.
//

#ifndef FINAL_PROJECT_GAMELOOP_H
#define FINAL_PROJECT_GAMELOOP_H

#endif //FINAL_PROJECT_GAMELOOP_H

#pragma once

#include <stdint-gcc.h>
#include "Player.h"
//#include "PlayerOptions.h"

class Game
{
private:
    //Player m_player;

    void WelcomeScreen();
    int Menu();
    void PlayerInfo(int selection);
    int TakeTurn();
public:

    void RunGame();
};