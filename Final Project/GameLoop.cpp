//
// Created by sdoherty on 10/11/19.
//


#include <iostream>
#include <fstream>
#include <string>
#include <ctime>
#include <vector>

#include "GameLoop.h"
#include "Player.h"

using namespace std;

//int diceRoll[6];

//declare a function to handle rolling the dice.
//this function has a parameter to account for the number of dice in play.
int* roll(int diceInPlay)
{
    //create an array of 6 ints
    int* roll = new int[6];
    cout << "ROLLING DICE!"  << endl <<  endl;
    //iterate array and assign random values from 1-6 to each member
    for (int a = 0; a < diceInPlay; a++)
    {
        roll[a]= rand() % 6 + 1;

    }
    //return array of 6 random ints from 1-6
    return roll;
}

//this function evaluates the roll updates player scores.
int DiceScore()
{
    int rollPoints = 0;
    //start with 6 dice
    int diceInPlay = 6;
    //see the random int
    srand(time(NULL));
    //create a boolean to keep the turn going until it is declared over later in function
    bool turnOver = false;
    //start the turn to be active while the turnover value is false.
    while (!turnOver)
    {
        //call roll function to get an array of number diceInPlay
        int* diceRoll = roll(diceInPlay);
        //set boolean value for 3 of a kind
        bool threeOfAKind = false;
        //set a value for triple die scoring
        int tripleDie = 0;
        // display output and check for 3 of a kind.
        cout << "Your roll :" << endl;
        for (int a = 0; a < diceInPlay; a++)
        {
            cout << "Die " << 1 + a << " " << diceRoll[a] << " " << endl;
        }

        //Triple Roll Check
        for (int a = 0; a < diceInPlay; a++)
        {
            //check values of rest of dice
            for (int b = ( a + 1); b < diceInPlay; b++)
            {
                //if 2 die match
                if (diceRoll[b] == diceRoll[a])
                {
                    //check for the 3rd match
                    for (int c = (b + 1); c < diceInPlay; c++)
                    {
                        //if 3 of a kind
                        if (diceRoll[c] == diceRoll[a])
                        {
                            //set bool to true
                            threeOfAKind = true;
                            //set multiplier value
                            tripleDie = diceRoll[c];

                            /*Note: Issue 3 of a kinds will automatically roll again
                            *if you do not save this score and roll a farkle you will lose. Example [1,1,1,5,6,6]
                            *added save points into game play to workaround.*/

                            //set all values to zero to account for score(AUTOMATICALLY REMOVE AND SCORE 3 OF A KIND)
                            diceRoll[a] = diceRoll[b] = diceRoll[c] = 0;
                        }
                    }
                }
            }
        }
        cout << endl;

        //TRIPLE ROLL SCORING
        if (threeOfAKind)
        {
            int tripleScore = 0;
            //triple 1 roll
            if (tripleDie == 1)
            {
                //worth 1000 points
                tripleScore = 1000;
            }
            //any other 3 of a kind
            else
            {
                //multiply roll * 100
                tripleScore = tripleDie * 100;
            }
            //communicate to player
            cout << "Three " << tripleDie << "'s of a kind!" << endl;
            cout << "adding " << tripleScore << " to your total!" << endl << endl;
            //add triple score to total
            rollPoints = tripleScore + rollPoints;

            //Debug note: save point.
            char choice;
            cout << "Stop now and save score?(Y/N)" << endl;
            cin >> choice;
            if ( choice == 'Y' || choice == 'y')
            {
                cout << "Saving Total Score" << endl;
                break;
            }
            else
            {
            }
            //redisplay dice with 3 of a kind removed.
            cout << "Lets see what's left" << endl;
        }

        //SPECIAL DIE SCORING
        //Dies 1 and 5 are handled differently.
        //check for special values in rest of roll
        for (int a = 0; a < diceInPlay; a++)
        {
            //die of value 1 is special. assign 100 points and take die out of remaining dice
            if (diceRoll[a] == 1)
            {
                char choice;
                //display value
                cout << "Die " << 1 + a << " is a " << diceRoll[a] << " " << endl;
                cout << "Die " << 1 + a << " is worth 100 points. " << endl;
                //ask for input if player wants to keep die
                cout << "Keep " << diceRoll[a] << " and add 100 points to total?(Y/N) " << endl;
                cin >> choice;
                if (choice == 'Y' || choice == 'y')
                {
                    //add 100 to points if keeping
                    rollPoints = rollPoints + 100;
                    // zero out the value of the remaining die
                    diceRoll[a] = 0;
                }
            }
            //5 roll: similar format as handling a 1 roll. assign 50 points and remove from remaining dice
            else if (diceRoll[a] == 5 )
            {
                char choice;
                cout << "Die " << 1 + a << " is a " << diceRoll[a] << " " << endl;
                cout << "Die " << 1 + a << " is worth 50 points. " << endl;
                cout << "Keep " << diceRoll[a] << " and add 50 points to total?(Y/N) " << endl;
                cin >> choice;
                if (choice == 'Y' || choice == 'y')
                {
                    rollPoints = rollPoints + 50;
                    diceRoll[a] = 0;
                }
            }
        }

        //HANDLING DICE LEFT IN PLAY
        //check roll account for dice left in roll
        //set the number of dice left to the number of dice in play
        int diceLeft = diceInPlay;
        //iterate through the dice and deduct for any die that have been kept
        for (int a = 0; a < diceInPlay; a++)
            {
                //if a scoring die has been rolled it has been set to 0
                if (diceRoll[a] == 0){
                    //remove that from available die to roll
                    --diceLeft;
                }
            }

        if ( diceInPlay == diceLeft)
        {
                //set turn over bool to true
                turnOver = true;
                cout << "FARKLE!" << endl;
                //score a zero for the roll
                rollPoints = 0;
                //exit
                break;

        }
        //BUG NOTE:  When there is 1 die left if you choose to keep the points, the die is set to zero and the
        //Game farkles and does not keep the score.
        //DONE:: fix farkle after last die is thrown.
        //BUG FIX : adding condition to address last die in roll.
        else if ( diceInPlay == 1 && diceLeft == 0 )
        {
            //set the correct number of dice left to roll
            diceInPlay = diceLeft;
            cout << "Last die"  << endl;
            cout << "There are " << diceLeft << " dice left to roll." << endl;
        }
        else
        {
            //set the correct number of dice left to roll
            diceInPlay = diceLeft;
            cout << "There are " << diceLeft << " dice left to roll." << endl;
        }
        cout << "Points total : " << rollPoints << endl;

        //SAVE SCORE CHECKPOINT
        //BUG FIX to keep from cycling through without input ask again to stop and save points.
        char choice;
        cout << "Stop now and save score?(Y/N)" << endl;
        cin >> choice;
        if ( choice == 'Y' || choice == 'y')
        {
            cout << "Saving Total Score" << endl;
            break;
        }
        else
        {
        }

    }
    //return score from this roll
    return rollPoints;
}

//this function takes a turn by calling DiceScore that rolls dice and returns points of the turn.
int Game::TakeTurn()
{
    int points;
    points = DiceScore();
    cout << "Total points for Turn: " << points << endl << endl;
    return points;
}

//On program start-up, it shall display the rules to the user as
// read from a text file submitted with the program.
void Game::WelcomeScreen()
{
    //create a string variable to store lines in the file
    string line;
    //pass rules file to filestream
    ifstream rulesFile ("rules.txt");
    cout << "got file" << endl;
    //Check whether state of stream is good("std::ifstream")
    while (rulesFile.good())
    {
        //output line to the console
        cout << line;
        //Extracts characters from the stream, as unformatted input:("std::istream::get")
        line = rulesFile.get();
    }
    //close file
    rulesFile.close();

    cout << endl << endl;
}
//The user can then set up the game by entering the number of players.
int Game::Menu()
{
    //solicit user for number of players
    unsigned int numOfPlayers;
    cout << "Enter Number of Players: or 0 to quit" << endl;
    cin >> numOfPlayers;
    //return value to be used in rungame function
    return numOfPlayers;
}

//this is the main game loop that handles the setup, plays and determining winner.
void Game::RunGame() {
    //call the welcome screen
    WelcomeScreen();
    // set bool to true to start game
    bool startGame = true;
    // Any number below two shall ask the user to add more players.
    while (startGame == true)
    {   //get the selection from the user by calling menu function
        int selection = Menu();
        //handle the selection by either staring the game, asking for more players or quitting
        if (selection > 1 )
        {
            //PLAY FARKLE
            //create a vector from class Player for player values
            vector<Player> players(selection);
            //cycle trough array of players
            for (int a = 0; a < players.size(); a++ )
            {
                //accept input and store as player name
                string playerName;
                cout << "Enter player " << a + 1  << " name." << endl;
                cin >> playerName;
                //update vector and create player object with name.
                players[a] = Player(playerName);
            }
            //begin play by cycling through players
            //create a bool to set that there is not a winner
            bool noWinner = true;
            //as long as there is no winner we play.
            while (noWinner){
                //DEBUG note: a did not progress properly without resetting the value after each player cycle.
                //DEVEL Note, possibly make new function to handle
                int a = 0;
                //iterate players vector
                for (a; a < players.size(); a++ )
                {
                    //use player for each turn
                    string playerName;
                    //bring in object
                    playerName = players[a].GetName();
                    cout << playerName << " 's TURN!" << endl;

                    //show players current score.
                    int playerTotal;
                    playerTotal = players[a].GetScore();
                    cout << playerName << " has " << playerTotal << " points. " << endl;

                    //get points from players turn
                    int points;
                    points = TakeTurn();
                    if ( points > 0 ) {
                        //configure ability to enter score when roll <1000.
                        //DEBUG NOTE: Dropping to 500 entry points
                        if (points >= 500) {
                            players[a].SetAbleToScore();
                        }
                        //See Debug scoring mechanics notes.. addition was not working correctly. took picture
                        if (players[a].GetAbleToScore()) {   //add to total
                            players[a].SetScore(points);
                        }

                        //display total points
                        cout << players[a].GetName() << "'s score is: " << players[a].GetScore() << endl << endl;
                        //check to see if the score changes pushes over 10000 points.
                        if (players[a].GetScore() >= 10000) {
                            cout << "A WINNER IS " << players[a].GetName() << endl << endl;
                            noWinner = false;
                            break;
                        }
                    }
                }
            }
            break;

        } else if (selection > 0) {
            //prompt for more players
            cout << "Farkle needs 2 or more players. Please add more." << endl;
            //being evaluating again
            continue;
        } else {
            //say goodbye
            cout << "Good Bye!" << endl;
            //choose to end game
            startGame = false;
        }
    }
}